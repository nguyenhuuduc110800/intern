import { types } from 'mobx-state-tree';
import { createContext } from 'react';
import { TodosStore } from './model/todo';
import { TodoDetailStore } from './model/todoDetails';

const CreatorStore = types.compose(TodosStore, TodoDetailStore);

const root = CreatorStore.create({
  todos: [],
  todo: { task: '', status: 'Created' },
});

const RootContext = createContext(root);
export default RootContext;
