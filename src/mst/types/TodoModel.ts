import { Instance, SnapshotIn, types } from 'mobx-state-tree';

export const TodoModel = types
  .model({
    task: types.optional(types.string, ''),
    status: types.optional(types.string, ''),
    _id: types.maybe(types.string),
  })
  .actions((self) => {
    const setTask = (value:string) => {
      self.task = value;
    };

    const setStatus = (value:string) => {
      self.status = value;
    };
    return { setStatus, setTask };
  });

export type TTodo = Instance<typeof TodoModel>;
export type TTodoIn = SnapshotIn<typeof TodoModel>;
