import axiosClient from 'src/config/axiosClient';

import { types, cast, flow } from 'mobx-state-tree';
import { TodoModel, TTodo, TTodoIn } from '../types/TodoModel';

export const TodosStore = types
  .model({ todos: types.array(TodoModel) })
  .actions((self) => {
    const getTodo = flow(function* getTodo() {
      try {
        const res = yield axiosClient.get('/api/todo');
        self.todos = res.data.data;
      } catch (error) {
        console.log(error);
      }
    });

    const cloneTodo = flow(function* cloneTodo(id: string) {
      try {
        const res: { data: { data: TTodo }} = yield axiosClient.post(`/api/todo/${id}/clone`);
        const index = self.todos.findIndex((e) => e._id === id);
        self.todos.splice(index + 1, 0, res.data.data);
      } catch (error) {
        console.log(error);
      }
    });

    const addTodo = flow(function* (data: string) {
      try {
        const res: { data : { data: TTodo }} = yield axiosClient.post('/api/todo', { task: data });
        self.todos = cast([...self.todos, res.data.data]);
      } catch (error) {
        console.log(error);
      }
    });

    const deleteTodo = flow(function* (id: string) {
      try {
        yield axiosClient.delete(`/api/todo/${id}`);
        const index = self.todos.findIndex((e) => e._id === id);
        self.todos.splice(index, 1);
      } catch (error) {
        console.log(error);
      }
    });

    const editTodo = flow(function* (id: string, data:TTodoIn) {
      try {
        yield axiosClient.put(`/api/todo/${id}`, data);
      } catch (error) {
        console.log(error);
      }
    });

    return {
      getTodo, cloneTodo, addTodo, deleteTodo, editTodo,
    };
  });
