import { types, flow } from 'mobx-state-tree';
import axiosClient from 'src/config/axiosClient';
import { TodoModel } from '../types/TodoModel';

export const TodoDetailStore = types
  .model({ todo: types.optional(TodoModel, { task: '', status: '' }) })
  .actions((self) => {
    const getOneTodo = flow(function* (id: string) {
      try {
        const res = yield axiosClient.get(`/api/todo/${id}`);
        self.todo = res.data.data;
      } catch (error) {
        console.log(error);
      }
    });
    return { getOneTodo };
  });
