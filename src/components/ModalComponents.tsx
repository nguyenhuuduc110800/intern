import React, { useContext } from 'react';
import { TextContainer, Modal } from '@shopify/polaris';
import { TTodo } from 'src/types/TTodo';
import RootContext from 'src/mst';

interface Iprops {
  itemTemp:TTodo,
  handleChange:()=>void,
  setItemTemp:Function,
  active:boolean
}

const ModalComponents:React.FC<Iprops> = (props) => {
  const { itemTemp, handleChange, setItemTemp, active } = props;
  const { deleteTodo } = useContext(RootContext);
  function onDelete() {
    deleteTodo(itemTemp._id);
    handleChange();
    setItemTemp(null);
  }
  return (
    <Modal
      open={active}
      onClose={handleChange}
      title="Xác Nhận Xóa Item"
      primaryAction={{ content: 'Delete', onAction: onDelete, destructive: true }}
      secondaryActions={[{ content: 'Cancel', onAction: handleChange }]}
    >
      <Modal.Section>
        <TextContainer>
          <p>có muốn xóa Việc Cần Làm {itemTemp && itemTemp.task}</p>
        </TextContainer>
      </Modal.Section>
    </Modal>
  );
};

export default ModalComponents;
