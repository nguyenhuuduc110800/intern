import React from 'react';

const THead:React.FC = () => {
  // const { tablehead } = props;
  return (
    <thead>
      <tr>

        <th
          data-polaris-header-cell="true"
          className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn Polaris-DataTable__Cell--header"
          scope="col"
        >
          task name
        </th>
        <th
          data-polaris-header-cell="true"
          className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--numeric"
          scope="col"
        >
          Status
        </th>
        <th
          data-polaris-header-cell="true"
          className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--header Polaris-DataTable__Cell--firstColumn"
          scope="col"
        >
          Actions
        </th>
      </tr>
    </thead>
  );
};

export default THead;
