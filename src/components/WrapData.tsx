import React, { ReactNode } from 'react';
import Link from 'next/link';

interface IWarpProps {
  children: ReactNode;
  title: string;
}
const WrapData: React.FC<IWarpProps> = (props) => {
  const { title, children } = props;
  return (

    <div className="Polaris-Page">
      <div
        className="Polaris-Page-Header Polaris-Page-Header--isSingleRow Polaris-Page-Header--mobileView Polaris-Page-Header--noBreadcrumbs Polaris-Page-Header--mediumTitle"
        style={{ marginBottom: '15px' }}
      >
        <div className="Polaris-Page-Header__Row">
          <div className="Polaris-Page-Header__TitleWrapper">
            <div>
              <div
                className="Polaris-Header-Title__TitleAndSubtitleWrapper"
              >
                <div>
                  <div
                    className="Polaris-Card"
                    style={{
                      width: '100%', display: 'flex', height: '100px', alignItems: 'center', justifyContent: 'space-between',
                    }}
                  >
                    <div className="Polaris-Card__Header" style={{ padding: 0, marginLeft: '15px' }}>
                      <h2 className="Polaris-Heading">{title}</h2>
                    </div>
                    { title === 'Todo List' && (
                    <Link href="/todo-app/add">
                      <a style={{ marginRight: '15px' }}>
                        <button className="Polaris-Button" type="button">
                          <span className="Polaris-Button__Content">
                            <span className="Polaris-Button__Text">
                              Add
                            </span>
                          </span>
                        </button>
                      </a>
                    </Link>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="Polaris-Card">
        <div className="">
          <div className="Polaris-DataTable__Navigation">
            <button
              className="Polaris-Button Polaris-Button--disabled Polaris-Button--plain Polaris-Button--iconOnly"
              aria-label="Scroll table left one column"
              type="button"
              disabled
            >
              <span className="Polaris-Button__Content">
                <span className="Polaris-Button__Icon">
                  <span className="Polaris-Icon">
                    <svg
                      viewBox="0 0 20 20"
                      className="Polaris-Icon__Svg"
                      focusable="false"
                      aria-hidden="true"
                    >
                      <path d="M12 16a.997.997 0 0 1-.707-.293l-5-5a.999.999 0 0 1 0-1.414l5-5a.999.999 0 1 1 1.414 1.414L8.414 10l4.293 4.293A.999.999 0 0 1 12 16z" />
                    </svg>
                  </span>
                </span>
              </span>
            </button>
            <button
              className="Polaris-Button Polaris-Button--plain Polaris-Button--iconOnly"
              aria-label="Scroll table right one column"
              type="button"
            >
              <span className="Polaris-Button__Content">
                <span className="Polaris-Button__Icon">
                  <span className="Polaris-Icon">
                    <svg
                      viewBox="0 0 20 20"
                      className="Polaris-Icon__Svg"
                      focusable="false"
                      aria-hidden="true"
                    >
                      <path d="M8 16a.999.999 0 0 1-.707-1.707L11.586 10 7.293 5.707a.999.999 0 1 1 1.414-1.414l5 5a.999.999 0 0 1 0 1.414l-5 5A.997.997 0 0 1 8 16z" />
                    </svg>
                  </span>
                </span>
              </span>
            </button>
          </div>
          { children }
        </div>
      </div>
    </div>
  );
};

export default WrapData;
