import {
  Button, FormLayout, Form, TextField, Select,
} from '@shopify/polaris';
import React, { FormEvent, useEffect, useContext } from 'react';
import Link from 'next/link';
import { NextPage, NextPageContext } from 'next';
import { observer } from 'mobx-react';

import router from 'next/router';
import RootContext from 'src/mst';
import WrapData from '../../components/WrapData';

const options = [
  { label: 'Created', value: 'Created' },
  { label: 'Doing', value: 'Doing' },
  { label: 'Done', value: 'Done' },
];

type TProps = {
  id: string;
};

const EditTodoApp: NextPage<TProps> = ({ id }) => {
  const { todo, editTodo, getOneTodo } = useContext(RootContext);
  async function getdata() {
    await getOneTodo(id);
  }
  useEffect(() => {
    try {
      getdata();
    } catch (error) {
      console.log(error);
    }
  }, []);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    await editTodo(id, { task: todo.task, status: todo.status });
    router.push('/todo-app');
  }
  return (
    <WrapData title="Edit Task">
      <div style={{ margin: '25px', padding: '25px 0' }}>
        <Form onSubmit={onSubmit}>
          <FormLayout>
            <TextField
              label="Task"
              value={todo.task}
              name="task"
              onChange={todo.setTask}
            />
            <Select
              label="Data"
              options={options}
              value={todo.status}
              onChange={todo.setStatus}
            />
            <div>
              <Button submit>EditTask</Button>
              <Link href="/todo-app">
                <a>
                  <button className="Polaris-Button" type="button">
                    <span className="Polaris-Button__Content">
                      <span className="Polaris-Button__Text">Turn Back</span>
                    </span>
                  </button>
                </a>
              </Link>
            </div>
          </FormLayout>
        </Form>
      </div>
    </WrapData>
  );
};

EditTodoApp.getInitialProps = async (ctx: NextPageContext) => {
  return { id: ctx.query.id as string };
};

export default observer(EditTodoApp);
