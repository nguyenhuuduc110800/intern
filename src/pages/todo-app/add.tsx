import React, { Button, FormLayout, Form, TextField } from '@shopify/polaris';
import { FormEvent, useContext, useEffect } from 'react';
import router from 'next/router';
import Link from 'next/link';
import { observer } from 'mobx-react';
import RootContext from 'src/mst';
import WrapData from '../../components/WrapData';

const AddTodo = () => {
  const { todo, addTodo } = useContext(RootContext);

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    await addTodo(todo.task);
    todo.setTask('');
    router.push('/todo-app');
  }
  useEffect(() => {
    todo.setTask('');
  }, []);
  return (
    <WrapData title="Add Data">
      <div style={{ margin: '25px', padding: '25px 0' }}>
        <Form onSubmit={onSubmit}>
          <FormLayout>
            <TextField
              label="Task"
              value={todo.task}
              name="task"
              onChange={todo.setTask}
            />
            <div>
              <Button submit>Add New Task</Button>
              <Link href="/todo-app">
                <a>
                  <button className="Polaris-Button" type="button">
                    <span className="Polaris-Button__Content">
                      <span className="Polaris-Button__Text">Turn back</span>
                    </span>
                  </button>
                </a>
              </Link>
            </div>
          </FormLayout>
        </Form>
      </div>
    </WrapData>
  );
};
export default observer(AddTodo);
