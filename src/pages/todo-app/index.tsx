import React, { useContext, useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, ButtonGroup } from '@shopify/polaris';
import { observer } from 'mobx-react';

import { TTodo } from 'src/types/TTodo';
import THead from 'src/components/THead';
import ModalComponents from 'src/components/ModalComponents';
import RootContext from 'src/mst';

import WrapData from '../../components/WrapData';

const Todoapp:React.FC = () => {
  const [active, setActive] = useState(false);
  const [itemTemp, setItemTemp] = useState<TTodo>({});
  const RootStore = useContext(RootContext);
  const { getTodo, todos, cloneTodo } = RootStore;
  useEffect(() => {
    getTodo();
  }, []);

  const handleChange = () => setActive(!active);

  return (
    <>
      <WrapData title="Todo List">
        <div className="Polaris-DataTable">
          <div className="Polaris-DataTable__ScrollContainer">
            <table className="Polaris-DataTable__Table">
              <THead />
              <tbody>
                {todos.map((element) => (
                  <tr
                    className="Polaris-DataTable__TableRow Polaris-DataTable--hoverable"
                    key={element._id}
                  >
                    <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--firstColumn">
                      {element.task}
                    </td>
                    <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                      {element.status}
                    </td>
                    <td className="Polaris-DataTable__Cell Polaris-DataTable__Cell--verticalAlignTop Polaris-DataTable__Cell--numeric">
                      <ButtonGroup spacing="tight">
                        <Link href={`/todo-app/${element._id}`}>
                          <span>
                            <Button>Edit</Button>
                          </span>
                        </Link>
                        <Button onClick={() => { cloneTodo(element._id as string); }}>
                          Clone
                        </Button>
                        <Button onClick={() => { handleChange(); setItemTemp(element); }} destructive>
                          Delete
                        </Button>
                      </ButtonGroup>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </WrapData>
      <ModalComponents active={active} handleChange={handleChange} setItemTemp={setItemTemp} itemTemp={itemTemp} />
    </>
  );
};

export default observer(Todoapp);
