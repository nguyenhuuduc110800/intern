import { AppProps } from 'next/dist/next-server/lib/router/router';
import { AppProvider } from '@shopify/polaris';
import { useRouter } from 'next/router';
import '@shopify/polaris/dist/styles.css';
import './Global.scss';
import React, { useEffect } from 'react';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    const user = localStorage.getItem('userId');
    console.log(user);
    if (user === null) {
      router.push('/auth/login');
    }
  }, []);
  return (
    <AppProvider i18n={{}}>
      <Component {...pageProps} />
    </AppProvider>
  );
}

export default MyApp;
