import React, { FormEvent, useState } from 'react';
import { Button, FormLayout, Form, TextField } from '@shopify/polaris';
import Link from 'next/link';
import { TUser } from 'src/types/TUser';
import axiosClient from '../../../config/axiosClient';
import styles from './styles.module.scss';

const register = () => {
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    axiosClient
      .post('api/auth/register', { username, password } as TUser)
      .then((res) => {
        console.log(res);
      }).catch((err) => {
        console.log(err);
      });
  }
  return (
    <div className={styles.wapper}>
      <Form onSubmit={onSubmit}>
        <FormLayout>
          <TextField
            label="Username"
            value={username}
            name="username"
            onChange={setUsername}
          />
          <TextField
            label="Password"
            type="password"
            value={password}
            name="password"
            onChange={setPassword}
          />
          <div className={styles.align_c}>
            <Button submit>Register</Button>
            to login page? Click
            <Link href="/auth/login">
              <a>Here</a>
            </Link>
          </div>
        </FormLayout>
      </Form>
    </div>
  );
};

export default register;
