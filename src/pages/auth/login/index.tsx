import React, { FormEvent, useEffect, useState } from 'react';
import { Button, FormLayout, Form, TextField } from '@shopify/polaris';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { TUser } from 'src/types/TUser';
import axiosClient from '../../../config/axiosClient';
import styles from './styles.module.scss';

const login = () => {
  const router = useRouter();
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [isAuth, setIsAuth] = useState(null);
  useEffect(() => {
    setIsAuth(localStorage.getItem('userId'));
  }, []);
  useEffect(() => {
    if (isAuth !== null) {
      router.push('/todo-app');
    }
  }, [isAuth]);
  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    try {
      const res = await axiosClient.post('api/auth/login', { username, password } as TUser);
      localStorage.setItem('userId', res.data.userId);
      router.push('/todo-app');
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <>
      <div className={styles.wapper}>
        <Form onSubmit={onSubmit}>
          <FormLayout>
            <TextField
              label="Username"
              value={username}
              name="username"
              onChange={setUsername}
            />
            <TextField
              label="Password"
              value={password}
              name="password"
              onChange={setPassword}
            />
            <div className={styles.align_c}>
              <Button submit>Login</Button>
              not have account? register{' '}
              <Link href="/auth/register">
                <a>Here</a>
              </Link>
            </div>
          </FormLayout>
        </Form>
      </div>
    </>
  );
};

export default login;
