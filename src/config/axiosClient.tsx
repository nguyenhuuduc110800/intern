import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

interface ConnectType {
  baseUrl?: string;
  LoginType?: string;
  Param?: object;
}

const connecttype: ConnectType = { baseUrl: 'http://localhost:8000' };

const axiosClient: AxiosInstance = axios.create({ baseURL: connecttype.baseUrl });

axiosClient.interceptors.request.use((config: AxiosRequestConfig) => {
  const Token = localStorage.getItem('userId');
  if (Token) {
    if (!config.headers.userId) {
      config.headers.userId = Token;
    }
  }
  return config;
});

axiosClient.interceptors.response.use((value: AxiosResponse) => {
  return value;
});

export default axiosClient;
