import { model, Model, Schema } from 'mongoose';
import { IUser } from '../@types/IUser';
// import * as jwt from "jsonwebtoken";
// import * as bcrypt from "bcryptjs";
const userSchema = new Schema<IUser>({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  },
});

// userSchema.pre("save", async function (next) {
//     if (!this.isModified("password")) {
//         next();
//     }
//     const salt = await bcrypt.genSalt(10);
//     this.password = await bcrypt.hash(this.password, salt);
// });

// userSchema.methods.GetSignJwtToken = function () {
//     return jwt.sign({ id: this._id }, "aks", {
//         expiresIn: "30d",
//     });
// };

// userSchema.methods.ComparePassword = async function (entrypass: string) {
//     return await bcrypt.compare(entrypass, this.password);
// };

const UserModel: Model<IUser> = model<IUser>('user', userSchema);

export default UserModel;
