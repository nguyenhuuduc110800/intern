import { Model, model, Schema } from 'mongoose';
import { ITodo } from '../@types/ITodo';

const Todoschema = new Schema<ITodo>({
  task: { type: String },
  status: {
    type: String,
    enum: ['Created', 'Doing', 'Done'],
    default: 'Created',
  },
  userId: { type: Schema.Types.ObjectId },
});

const TodoModel: Model<ITodo> = model<ITodo>('Todo', Todoschema);

export default TodoModel;
