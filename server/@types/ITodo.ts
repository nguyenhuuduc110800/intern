import { Document } from 'mongoose';

export interface ITodo extends Document {
  task: String;
  status: String;
}
