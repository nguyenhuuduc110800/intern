import { TTodo } from 'src/types/TTodo';
import { Context } from 'koa';
import KoaRouter from 'koa-router';
import TodoModel from '../model/TodoModel';

const api = new KoaRouter();

api.get('/api/todo', async (ctx: Context) => {
  const { userid } = ctx.request.headers;
  const data = await TodoModel.find({ userId: userid }).lean();
  ctx.body = { success: true, data };
});

api.post('/api/todo', async (ctx: Context) => {
  const { userid } = ctx.request.headers;
  const { task } = ctx.request.body as TTodo;
  const data = new TodoModel({ task, userId: userid });
  const res = await data.save();
  ctx.body = { data: res };
});

api.get('/api/todo/:id', async (ctx: Context) => {
  const { id } = ctx.params;
  if (!id) ctx.throw(404);
  const todo = await TodoModel.findById(id).lean();
  if (!todo) ctx.throw(404);
  ctx.body = { success: true, data: todo };
});

api.put('/api/todo/:id', async (ctx: Context) => {
  const { userid } = ctx.request.headers;
  const { task, status } = ctx.request.body as TTodo;
  const { id } = ctx.params;
  await TodoModel.findByIdAndUpdate(id, { task, status, userId: userid }).lean();
  ctx.status = 200;
});

api.post('/api/todo/:id/clone', async (ctx: Context) => {
  const { userid } = ctx.request.headers;
  const { id } = ctx.params;
  if (!id) ctx.throw(404);
  const res = await TodoModel.findById(id).lean();
  if (!res) ctx.throw(404);
  const data = new TodoModel({
    task: `${res.task} - Clone`,
    status: res.status,
    userId: userid,
  });
  const body = await data.save();
  ctx.body = { data: body };
});

api.delete('/api/todo/:id', async (ctx: Context) => {
  const { id } = ctx.params;
  if (!id) ctx.throw(404);
  await TodoModel.findByIdAndDelete(id).lean();
  ctx.status = 200;
});

export default api;
