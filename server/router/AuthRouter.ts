import { TUser } from 'src/types/TUser';
import { Context } from 'koa';
import KoaRouter from 'koa-router';
import UserModel from '../model/UserModel';

const api = new KoaRouter();

api.post('/api/auth/login', async (ctx: Context) => {
  const { username, password } = ctx.request.body as TUser;

  const user = await UserModel.findOne({ username, password });

  if (!user) {
    ctx.throw(404);
  }

  ctx.body = { userId: user._id };
});

api.post('/api/auth/register', async (ctx: Context) => {
  const { username, password } = ctx.request.body as TUser;

  const result = await UserModel.findOne({ username });
  if (result) {
    ctx.status = 401;
  }

  const data = new UserModel({ username, password });
  await data.save();
  ctx.status = 200;
});

export default api;
