import http from 'http';
import next from 'next';
import Koa, { Context } from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import DataBase from './config/Database';

/* ---------------------------- import Api Router --------------------------- */
import AuthRouter from './router/AuthRouter';
import TodoRouter from './router/TodoRouter';

const database = new DataBase();
database.ConnectDB();

// Koa
const KoaApp = new Koa();
const server = http.createServer(KoaApp.callback());
const router = new Router();
// next
const routerForNext = new Router();
const nextApp = next({ dev: true });
const nextHandler = nextApp.getRequestHandler();

// middlewares
KoaApp.use(bodyParser());

KoaApp.use(AuthRouter.routes());
KoaApp.use(TodoRouter.routes());

const start = async () => {
  await nextApp.prepare();
  KoaApp.use(router.routes());
  KoaApp.use(routerForNext.routes());
  routerForNext.get('(.*)', async (ctx: Context) => {
    await nextHandler(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  server.listen(8000, () => {
    console.info('Server is running in http://localhost:8000');
  });
};

// thực thi luôn
(() => start())();
