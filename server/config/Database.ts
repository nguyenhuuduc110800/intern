import { connect, ConnectionOptions } from 'mongoose';

export default class DataBase {
  private MongoURI: string =
    <string>process.env.MongoURI
    || 'mongodb+srv://ducnguyen:fa123123@cluster0.mjfnf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

  private MongoDBConnectOption: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };

  public ConnectDB = async () => {
    await connect(this.MongoURI, this.MongoDBConnectOption).then(
      (conn) => {
        const { port, host } = conn.connection;
        console.log(`
        MongoDB Connected On Host: ${host}
        MongoDB Connected On Port: ${port}
        `);
      },
    );
  };
}
